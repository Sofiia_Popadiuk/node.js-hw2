const Note = require('../Note')
const User = require('../User')

class AuthController {
    async getNotes(req, res) {
        try {
            const offset = +req.query.offset || 0
            const limit = +req.query.limit || 0

            const allNotes = await Note.find({ userId: req.user.id })
            const notes = await Note.find({ userId: req.user.id })
                .skip(offset)
                .limit(limit).select('-__v')

            return res.status(200).send({
                offset: offset,
                limit: limit,
                count: allNotes.length,
                notes: notes
            })
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async addNote(req, res) {
        try {
            const textInfo = req.body.text
            const user = await User.findOne({ _id: req.user.id })

            if (!textInfo) {
                return res.status(400).send({ message: "Error message" });
            }

            const note = new Note({
                userId: user.id,
                completed: false,
                text: textInfo,
                createdDate: new Date()
            })
            await note.save()
            console.log('Note created')

            return res.status(200).send({ message: 'Success' })
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async getNoteById(req, res) {
        try {
            const note = await Note.findOne({ _id: req.params.id }).select('-__v')
            if (note) {
                console.log('Get note by id success')
                return res.status(200).send({ note: note })
            } else {
                console.log('No note with this id')
                return res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async updateNoteById(req, res) {
        try {
            if (await Note.findOneAndUpdate({ _id: req.params.id }, { text: req.body.text })) {
                console.log('Note update success')
                return res.status(200).send({ message: 'Success' })
            } else {
                console.log('No note with this id')
                return res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async checkNoteById(req, res) {
        try {
            const note = await Note.findOne({ _id: req.params.id })
            const completedValue = note.completed
            if (await Note.findOneAndUpdate({ _id: req.params.id }, { completed: !completedValue })) {
                console.log('check/uncheck note success')
                return res.status(200).send({ message: "Success" })
            } else {
                console.log('No note with this id')
                return res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async deleteNoteById(req, res) {
        try {
            if (await Note.findOneAndDelete({ _id: req.params.id })) {
                console.log('Note deleted success')
                return res.status(200).send({ message: 'Success' })
            } else {
                console.log('No note with this id')
                return res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }
}

module.exports = new AuthController()