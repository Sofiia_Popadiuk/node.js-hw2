const bcrypt = require("bcryptjs");
const User = require('../User')
const jwt = require('jsonwebtoken')
const { secret } = require('../config/config')

const generateToken = (id, username) => {
    const payload = { id, username }
    return jwt.sign(payload, secret, { expiresIn: '24h' })
}

class AuthController {
    async registration(req, res) {
        try {
            const body = req.body

            if (!(body.username && body.password)) {
                return res.status(400).send({ message: "Error message" });
            }

            if (await User.findOne({ username: body.username })) {
                return res.status(400).send("Error message");
            }

            const salt = await bcrypt.genSalt(10)
            const hashPassword = await bcrypt.hash(body.password, salt)
            const user = new User({ username: body.username, password: hashPassword, createdDate: new Date() })
            await user.save()
            console.log('User created')

            return res.status(200).send({ message: 'Success' })
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async login(req, res) {
        try {
            const body = req.body

            if (!(body.username && body.password)) {
                return res.status(400).send({ message: "Error message" });
            }

            const user = await User.findOne({ username: body.username })
            if (user) {
                const validPassword = await bcrypt.compare(body.password, user.password);
                if (validPassword) {
                    const token = generateToken(user._id, user.username)
                    console.log('Login success')
                    return res.status(200).send({ message: "Success", jwt_token: token });
                } else {
                    res.status(400).send({ message: "Error message" });
                    console.log('Password is invalid')
                }
            } else {
                res.status(400).send({ message: "Error message" });
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }
}

module.exports = new AuthController()