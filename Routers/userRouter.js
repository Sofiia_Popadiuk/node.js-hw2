const Router = require('express')
const router = new Router()
const controller = require('../Controllers/userController')
const authMiddleware = require('../middleware/authMiddleware')

router.get('/me', authMiddleware, controller.getProfile)
router.delete('/me', authMiddleware, controller.delete)
router.patch('/me', authMiddleware, controller.changePassword)

module.exports = router