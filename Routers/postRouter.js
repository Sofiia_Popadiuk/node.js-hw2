const Router = require('express')
const router = new Router()
const controller = require('../Controllers/postController')
const authMiddleware = require('../middleware/authMiddleware')

router.get('/notes', authMiddleware, controller.getNotes)
router.post('/notes', authMiddleware, controller.addNote)
router.get('/notes/:id', controller.getNoteById)
router.put('/notes/:id', controller.updateNoteById)
router.patch('/notes/:id', controller.checkNoteById)
router.delete('/notes/:id', controller.deleteNoteById)

module.exports = router