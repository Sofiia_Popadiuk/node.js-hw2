const jwt = require('jsonwebtoken')
const {secret} = require('../config/config')

module.exports = function (req, res, next) {
    try {
        const token = req.headers.authorization.split(' ')[1]
        if(!token){
            return res.status(400).send('User is not login')
        }
        const decodedData = jwt.verify(token, secret)
        req.user = decodedData
        next()
    } catch (error) {
        console.error(error)
        return res.status(400).send('User is not login')
    }
}