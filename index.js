const express = require('express');
const path = require('path');
const authRouter = require('./Routers/authRouter');
const userRouter = require('./Routers/userRouter');
const postRouter = require('./Routers/postRouter');
const mongoose = require('mongoose');
const morgan = require('morgan')
const app = express();
const PORT = 8080;
const {mongoDb} = require('./config/config')

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'))
})

app.listen(PORT, () => {
    console.log('Server START...')
})

app.use(morgan('\n:method :url :status :res[content-length] - :response-time ms'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
app.use('/api/auth', authRouter)
app.use('/api/users', userRouter)
app.use('/api', postRouter)

const start = async () => {
    try {
        mongoose.connect(mongoDb)
        console.log('Connection with db success')
    } catch (error) {
        console.error(error)
    }
}

start()