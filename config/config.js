require('dotenv').config()

module.exports = {
    mongoDb: process.env.MONGODB_URI,
    secret: process.env.JWT_SECRET
}